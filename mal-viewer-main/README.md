<h1>InfoTech Mal-Viewer</h1>

<h3>Project description</h3>

Infotech Mal-Viewer is a web project made by 4 person team from InfoTech programming school, class III G.
The project is making use of the Angular framework and a variation of My Anime List API to access user data.

<h4>How do I access data from API?</h4>

By inserting the name of an already existing account on Mal-Viewer, users can access the list of watched animes, and its user ratings ranging from 10 to non-rated.
In case of entering a non-existent account name user receives a message that there is no such user.

<h4>Where can I find it?<h4>

The site is hosted through GitLab pages on https://infotech-g.gitlab.io/mal-viewer
