import { Component, OnInit } from '@angular/core';

import { ConnectionService } from 'src/app/connection.service';
import { Anime } from 'src/app/models/anime'
import { Request } from 'src/app/models/request'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  title = 'mal-viewer';
  animelist!: Anime[];
  loading: boolean = true;
  errorOccure: boolean = false;
  errorMessage = "Unexpected error";

  constructor(
    private ConnectionService: ConnectionService,
  ) {
  }

  ngOnInit() {
    this.ConnectionService.getAnimeList().subscribe( 
      (request) => {

        this.loading = false;

        if (request.request_hash == "400") {
          this.errorOccure = true
          this.errorMessage = "There's no such user"
        }
        else if (request.request_hash == "503") {
          this.errorOccure = true
          this.errorMessage = "Service is temporarily unaviable. Try again in a few minutes"
        }
        else {
          this.animelist = request.anime
        }
      }, 
    ) 
  }

  is_movie_rated(rate: string): boolean {
    if (parseInt(rate) !== 0) {
      return true
    } else {
      return false
    }
  }

  get_rate_color(rate: string) {
    if (parseInt(rate) >= 9) {
      return { background: "#00ff00" }
    } else if (parseInt(rate) >= 7) {
      return { background: "#80ff00" }
    } else if (parseInt(rate) >= 5) {
      return { background: "#f9f906" }
    } else if (parseInt(rate) >= 3) {
      return { background: "#ff8000" }
    } else {
      return { background: "#ff0000" }
    }
  }
}
