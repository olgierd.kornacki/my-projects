import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConnectionService } from 'src/app/connection.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild("video", { static: false })
  videoE?: ElementRef<HTMLVideoElement>

  constructor(
    private connectionService: ConnectionService,
    private router: Router,
  ) { }

  ngOnInit(): void { }

  parseUsername(username: string): void {
    this.connectionService.username = username
    this.router.navigateByUrl('/user')
  }
}
