import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from  '@angular/common/http';
import { Request } from './models/request'
import { Observable, of, Subscriber, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  public username: string = ""

  constructor(
    private http: HttpClient,
  ) { }

  public getAnimeList(): Observable<Request> {

    var error400: Request = {
      "request_hash": "400",
      "request_cached": false,
      "request_cache_expiry": 100,
      "anime": []
    }

    var error503: Request = {
      "request_hash": "503",
      "request_cached": false,
      "request_cache_expiry": 100,
      "anime": []
    }

    return this.http.get<Request>(`https://api.jikan.moe/v3/user/${this.username}/animelist`)
    .pipe(
      catchError(err => {
        if (err.status == "404") { return of(error400) }
        else if (err.status == "503") { return of(error503) }
        return of(error400)
      }),
    );
  }
}
