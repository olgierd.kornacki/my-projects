from Game.gameClass import *

gameObject = Game()

while gameObject.running:
    gameObject.current_menu.display_menu()
    gameObject.game_loop()
