import pygame
import random
from Game.settings import *


class Generating:
    def __init__(self, width=22, height=34):
        self.ListOfObjects = [0, 1, 2, 3, 4, 5]

        self.MapWidth = width
        self.MapHeight = height

    def Gener(self):
        Game_Map = []
        ap = []
        for lenght in range(self.MapWidth):
            ap = []
            WorldDistance = random.randint(0, 3)
            for width in range(self.MapHeight):
                distance = random.randint(2, 5)
                if width + distance > self.MapWidth:
                    distance = self.MapWidth - width
                if width != WorldDistance:
                    ap.append(0)
                else:
                    Enviroment = random.randint(0, len(self.ListOfObjects) - 1)
                    ap.append(self.ListOfObjects[Enviroment])
                    distance = random.randint(5, 20)
                    WorldDistance += distance
            Game_Map.append(ap)
        return Game_Map


class Obstacle(pygame.sprite.Sprite):
    def __init__(self, game, AxisX, AxisY):
        self.groups = game.all_sprites, game.walls
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = pygame.Surface((TILESIZE, TILESIZE))
        self.image.fill(WHITE)
        self.rect = self.image.get_rect()
        self.ObjectPosition = [AxisX, AxisY]
        self.rect.x = AxisX * TILESIZE
        self.rect.y = AxisY * TILESIZE

    def interaction(self, player):
        pass


class Rock(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(Rock, self).__init__(game, PositionX, PositionY)
        self.image.fill(LIGHTGREY)

    def interaction(self, player):
        new_value = player.materials["Stone"]
        new_value += 1
        player.materials["Stone"] = new_value
        player.hunger_current = player.lose_value(player.hunger_current, 1)


class Tree(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(Tree, self).__init__(game, PositionX, PositionY)
        self.image.fill(BROWN)

    def interaction(self, player):
        new_value = player.materials["Wood"]
        new_value += 1
        player.materials["Wood"] = new_value
        player.hunger_current = player.lose_value(player.hunger_current, 1)


class Grass(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(Grass, self).__init__(game, PositionX, PositionY)
        self.image.fill(GREEN)


class Iron(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(Iron, self).__init__(game, PositionX, PositionY)
        self.image.fill(IRON)

    def interaction(self, player):
        new_value = player.materials["Iron"]
        new_value += 1
        player.materials["Iron"] = new_value
        player.hunger_current = player.lose_value(player.hunger_current, 1)


class Gold(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(Gold, self).__init__(game, PositionX, PositionY)
        self.image.fill(GOLDEN)

    def interaction(self, player):
        new_value = player.materials["Gold"]
        new_value += 1
        player.materials["Gold"] = new_value
        player.hunger_current = player.lose_value(player.hunger_current, 1)


class AppleTree(Obstacle):
    def __init__(self, game, PositionX, PositionY):
        super(AppleTree, self).__init__(game, PositionX, PositionY)
        self.image.fill(ORANGERED)

    def interaction(self, player):
        player.hunger_current = player.gain_value(player.hunger_current, player.hunger_maximum, 1)
