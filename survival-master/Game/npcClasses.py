from Game.playerClasses import GameParticipant
import  random
from Game.settings import *

class Cow(GameParticipant):
    def __init__(self, display):
        GameParticipant.__init__(self, TILESIZE,TILESIZE, "Images\\cow.png", 100)
    def idle(self):
        move = random.randint(0,32)
        if(move == 0 ):
            dir = random.randint(0,4)
            if(dir == 0):
                self.AxisX_change += TILESIZE
            if (dir == 1):
                self.AxisX_change -= TILESIZE
            if (dir == 2):
                self.AxisY_change += TILESIZE
            if (dir == 3):
                self.AxisY_change -= TILESIZE
        if self.AxisY_change < 0:
            self.AxisY_change = 0
        elif self.AxisY_change > WIDTH - 32:
            self.AxisY_change = WIDTH - 32

        if self.AxisX_change > HEIGHT - 140:
            self.AxisX_change = HEIGHT - 140
        elif self.AxisX_change <= 0:
            self.AxisX_change = 0
