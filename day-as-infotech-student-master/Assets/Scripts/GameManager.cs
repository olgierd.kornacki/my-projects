﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private bool gameHasEnded = false;
    private float restartDelay = 2f;
    [SerializeField] private GameObject gameOver;

     public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            gameOver.SetActive(true);
            Invoke("Restart", restartDelay);      
        }


    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
