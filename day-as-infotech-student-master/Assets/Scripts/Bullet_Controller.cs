﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Controller : MonoBehaviour
{
    private Player_Shooting playerShootingScript;
    private int weaponDamage;

    private void Start()
    {
        playerShootingScript = GameObject.Find("Player").GetComponent<Player_Shooting>();
        weaponDamage = playerShootingScript.weaponDamage;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Enemy_Controller>().TakeDamage(weaponDamage, transform.up);
        }
        Destroy(this.gameObject);
    }
}
