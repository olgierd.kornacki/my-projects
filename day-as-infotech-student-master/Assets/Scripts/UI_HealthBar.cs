﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HealthBar : MonoBehaviour
{

    [SerializeField] Slider Slider;
    [SerializeField] private Image Fill;
    private int maxHealth;
    private Color red = new Color(255f / 255f, 0f / 255f, 0f / 255f);
    private Color green = new Color(0f / 255f, 255f / 255f, 0f / 255f);
    private Color yellow = new Color(255f / 255f, 255f / 255f, 0f / 255f);

    public void SetMaxHealth(int health)
    {
        Slider.maxValue = health;
        Slider.value = health;

        Fill.color = green;
    }

    public void SetHealth(int health)
    {
        Slider.value = health;
        
        if (health > 0.6*Slider.maxValue)
        {
            Fill.color = green; 
        }
        else if (health <= 0.6*Slider.maxValue && health > 0.2*Slider.maxValue)
        {
            Fill.color = yellow;
        }
        else if (health <= 0.2 * Slider.maxValue)
        {
            Fill.color = red;
        }
    }
}
