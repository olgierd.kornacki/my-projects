﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MinimapCamera : MonoBehaviour
{

    [SerializeField] private Transform player;

    private void LateUpdate()
    {
        Vector3 newPosition = player.position;
        newPosition.z = transform.position.z;
        transform.position = newPosition;
    }
}
