﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift_Controller : MonoBehaviour
{
    [SerializeField] private bool isPlayerClose;
    public Animator anim;
    public LayerMask PlayerMask;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        isPlayerClose = Physics2D.OverlapCircle(transform.position, 2f, PlayerMask);
        if (isPlayerClose == true)
        {
            anim.SetBool("PlayerClose", true);
        }
        if (isPlayerClose == false)
        {
            anim.SetBool("PlayerClose", false);
        }
    }
}
