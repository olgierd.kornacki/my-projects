﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Weapon_Controller : MonoBehaviour
{
    [SerializeField] private string WeaponName;
    [SerializeField] private int WeaponDamage;
    [SerializeField] private int AmmoUsage;
    [SerializeField] private int UsageStyle;

    [SerializeField] GameObject WeaponInfo;         //
    [SerializeField] GameObject WeaponDmgInfo;      //     <--  Stuff about Info Popup
    [SerializeField] GameObject WeaponAmmoUsageInfo;//
    [SerializeField] GameObject WeaponNameInfo;     //
    private TextMeshPro damageText, ammoText, nameText; //


    private bool isNearWeapon = false;
    private Player_Shooting playerShootingScript;
    private Player_Controller playerControllerScript;
    


    private void Start() 
    {
        playerShootingScript = GameObject.Find("Player").GetComponent<Player_Shooting>();
        playerControllerScript = GameObject.Find("Player").GetComponent<Player_Controller>();

        if (WeaponInfo)
        {
            ChangeWeaponInfo();
        }
    }

    private void Update()
    {
        if (isNearWeapon & Input.GetKeyDown(KeyCode.E))
        {
            ChangeWeapons(WeaponName, WeaponDamage, AmmoUsage, UsageStyle, this.gameObject);
        }
    }

    private void ChangeWeapons(string WeaponName, int WeaponDamage, int AmmoUsage, int UsageStyle, GameObject WeaponObject)
    {
        if (playerControllerScript.currentWeapon != "None")
        {
            playerControllerScript.equipedWeapon.transform.position = GameObject.Find("Player").transform.position;
            playerControllerScript.equipedWeapon.SetActive(true);
        }
        playerShootingScript.weaponDamage = WeaponDamage;
        playerShootingScript.ammoUsage = AmmoUsage;
        playerShootingScript.shootingStyle = UsageStyle;
        playerControllerScript.ChangeWeaponNameUI(WeaponName);
        playerControllerScript.EquipThisWeaponAsObject(WeaponObject);
        WeaponObject.SetActive(false);
    }

    private void ChangeWeaponInfo()
    {
        damageText = WeaponDmgInfo.GetComponent<TextMeshPro>();
        ammoText = WeaponAmmoUsageInfo.GetComponent<TextMeshPro>();
        nameText = WeaponNameInfo.GetComponent<TextMeshPro>();
        damageText.text = WeaponDamage.ToString();
        ammoText.text = AmmoUsage.ToString();
        nameText.text = WeaponName;
        if (UsageStyle == 2)
        {
            nameText.color = new Color32(000, 187, 045, 255);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isNearWeapon = true;
            WeaponInfo.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isNearWeapon = false;
            WeaponInfo.SetActive(false);
        }
    }
}
