﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemy_Controller : MonoBehaviour
{
    private int maxHealth = 50;
    private int currentHealth;

    [SerializeField] private int Enemy_DMG;
    [SerializeField] private GameObject AmmoBoxPrefab;
    [SerializeField] private bool isPlayerClose;
    [SerializeField] private LayerMask PlayerMask;
    [SerializeField] private GameObject DamagePopup; 
  //[SerializeField] private GameObject IndicatorsPoint;
    private TextMeshPro textMesh;
    
    int x = 0;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void Update()
    {

        if (currentHealth <= 0)
        {
            StartCoroutine(Delay());
        }
        isPlayerClose = Physics2D.OverlapCircle(transform.position, 0.5f, LayerMask.GetMask("Player"));
        if (isPlayerClose)
        {
            if (x == 0)
            {
                StartCoroutine(DealDMG());
            }
        }
    }

    IEnumerator Delay()
    {

        if (GameObject.Find("Player").GetComponent<Player_Shooting>().Player_Ammo < 70)
        {
            int randomINT = Random.Range(0, 2);
            yield return new WaitForSeconds(0.02f);
            if (randomINT == 0)
            {
                GameObject Spawned_Enemy = Instantiate(AmmoBoxPrefab, transform.position, Quaternion.identity) as GameObject;
                Spawned_Enemy.transform.parent = null;
            }
        }

       
        Destroy(this.gameObject);
    }

    IEnumerator DealDMG()
    {
        x++;
        GameObject.Find("Player").GetComponent<Player_Controller>().TakePlayerDamage(Enemy_DMG, transform.up);

        yield return new WaitForSeconds(0.75f);
        x = 0;
    }

    public void TakeDamage(int DMG, Vector2 Enemy_dir)
    {
        CreateDamagePopup(DMG);
        currentHealth -= DMG;
        GetComponent<Rigidbody2D>().AddForce(Enemy_dir *500f);
    }
    
    public void CreateDamagePopup(int DMG)
    {
        
        GameObject CreatedPopup = Instantiate(DamagePopup, new Vector3(this.transform.position.x,
                                                                       this.transform.position.y + 0.6f,
                                                                       this.transform.position.z),
                                                                       Quaternion.identity) as GameObject;      
        textMesh = CreatedPopup.GetComponent<TextMeshPro>();
        textMesh.text = "-" + DMG.ToString();        
    }
}
