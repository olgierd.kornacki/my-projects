﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    [SerializeField] private UI_HealthBar HealthBar;
    [SerializeField] private Text currentWeaponUI;
    [SerializeField] private Text playerMoneyText;
    public int maxHealth = 100;
    public int currentHealth;
    public int playerMoney = 100;
    public bool isNearWeapon = false;
    public string currentWeapon = "None";
    public GameObject equipedWeapon;

    private void Start()
    {
        ChangeWeaponNameUI(currentWeapon);
        HealthBar = GameObject.Find("HealthBar").GetComponent<UI_HealthBar>();
        currentHealth = maxHealth;
        HealthBar.SetMaxHealth(maxHealth);
        PlayerMoneyDisplay(playerMoney);
    }

    private void Update()
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    public void TakePlayerDamage(int EnemyDamage, Vector2 dir)
    {
        currentHealth -= EnemyDamage;
        GetComponent<Rigidbody2D>().AddForce(dir * 0.15f);
        HealthBar.SetHealth(currentHealth);
    }

    public void ChangeWeaponNameUI(string weaponName) 
    {
        currentWeapon = weaponName;
        currentWeaponUI.text = "Weapon: " + weaponName;
    }

    public void EquipThisWeaponAsObject(GameObject Weapon)
    {
        equipedWeapon = Weapon;
    }

    public void PlayerMoneyDisplay(int updatedPlayerMoney)
    {
        playerMoney = updatedPlayerMoney;
        playerMoneyText.text = "Money: " + playerMoney.ToString();
    }

    public void PlayerHealthUpdate(int updatedHealth)
    {
        if (updatedHealth <= maxHealth)
        {
            HealthBar.SetHealth(updatedHealth);
            currentHealth = updatedHealth;
        }
        else
        {
            HealthBar.SetHealth(maxHealth);
            currentHealth = maxHealth;
        }
    }
}
