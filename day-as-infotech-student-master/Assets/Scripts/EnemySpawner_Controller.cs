﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner_Controller : MonoBehaviour
{

    [SerializeField] private GameObject EnemyToSpawn;
    [SerializeField] private List<Transform> SpawnPoints;
    [SerializeField] private Transform currentSpawnPoint;
    private float Random_Spawn;

    public Text zombieText; // unnecessary
    public int zombieCounter = 0; // unnecessary

    void Start()
    {
        StartCoroutine(DelaySpawn());
    }


    void Update()
    {
        Random_Spawn = Random.Range(0, 4);
        zombieText.text = "Zombies: " + zombieCounter.ToString(); // unnecessary
    }

    public IEnumerator DelaySpawn()
    {
        while (true)
        {
            foreach (Transform T in SpawnPoints)
            {
                currentSpawnPoint = T;
                yield return new WaitForSeconds(4.5f);
                GameObject Spawned_Enemy = Instantiate(EnemyToSpawn, T.position, Quaternion.identity) as GameObject;
                Spawned_Enemy.GetComponent<Pathfinding.AIDestinationSetter>().target = GameObject.Find("Player").transform;

                zombieCounter += 1; // unnecessary
            }

        }
    }
}
