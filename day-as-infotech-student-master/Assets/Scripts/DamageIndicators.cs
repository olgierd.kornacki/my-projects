﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageIndicators : MonoBehaviour
{ 
    [SerializeField] private float animationSpeed;
    [SerializeField] private float disappearingTime;
    private float disappearingCounter;    

    private void Update()
    {
        DamageIndicatorAnimation();
        DamageIndicatorDestroyer();
    } 

    private void DamageIndicatorAnimation()
    {
        this.transform.position = new Vector3(this.transform.position.x,
                                           this.transform.position.y + animationSpeed * Time.deltaTime,
                                           this.transform.position.z);
    }  
    
    private void DamageIndicatorDestroyer()
    {
        if (disappearingCounter > disappearingTime)
        { Destroy(this.gameObject); }
        disappearingCounter += Time.deltaTime;
    }
}
