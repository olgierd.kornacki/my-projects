﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Holder : MonoBehaviour
{
    public Camera_Controller cameraFollow;
    public Transform playerTransform;
    void Start()
    {
        cameraFollow.Setup(() => Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
