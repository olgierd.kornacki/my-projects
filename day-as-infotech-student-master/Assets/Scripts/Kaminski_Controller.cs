﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Kaminski_Controller : MonoBehaviour
{
    [SerializeField] private Transform Player_Transform;
    [SerializeField] private Transform MissleFirePoint;
    [SerializeField] private float Angry_Range;
    [SerializeField] private Sprite Angry_Kaminski;
    [SerializeField] private GameObject TiltParticle;
    [SerializeField] private GameObject Missle;
    [SerializeField] private GameObject DamagePopup;
    private TextMeshPro textMesh;


    bool isPlayerClose = false;
    float ROF; //(Rate of Fire) Time to lunch next missle

    void Start()
    {

    }


    void Update()
    {

        if (isPlayerClose)
        {
            ROF += Time.deltaTime;
            GetComponent<BoxCollider2D>().enabled = false;
            transform.up = new Vector2(transform.position.x - Player_Transform.position.x, transform.position.y - Player_Transform.position.y) * -1;
            GetComponent<SpriteRenderer>().sprite = Angry_Kaminski;
            if (ROF >= 1.75f)
            {
                GameObject LinuxMissle = Instantiate(Missle, MissleFirePoint.position, Quaternion.identity) as GameObject;
                ROF = 0;
            }
        }
       
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            GameObject Tilt = Instantiate(TiltParticle, transform.position, Quaternion.identity) as GameObject;
            Destroy(Tilt, 2f);

            StartCoroutine(Delay());
        }
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        isPlayerClose = true;
    }
/*          // To nie działa nie wiem czemu totalnie nie odpala sie void wywala jakis blad (nie dziala jak dodajesz to kaminskiego tag enemy i strzelasz do goscia)
    public void TakeDamage(int DMG) { CreateDamagePopup(DMG); Debug.Log("xd"); }
    public void CreateDamagePopup(int DMG)
    {
        
        GameObject CreatedPopup = Instantiate(DamagePopup, new Vector3(this.transform.position.x,
                                                                       this.transform.position.y + 1.6f,
                                                                       this.transform.position.z),
                                                                       Quaternion.identity) as GameObject;      
        textMesh = CreatedPopup.GetComponent<TextMeshPro>();
        textMesh.text = "- " + DMG.ToString();        
    } */
}
