﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Rotation : MonoBehaviour
{
    void Update()
    {
        if (!UI_PauseMenu.GameIsPaused)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.up = new Vector2(transform.position.x - mousePos.x, transform.position.y - mousePos.y) * -1f;
        }
    }
}
