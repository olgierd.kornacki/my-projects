﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Shooting : MonoBehaviour
{
    [SerializeField] public int Player_Ammo;
    [SerializeField] private Transform Fire_Point;
    [SerializeField] private LineRenderer Line_Renderer;
    [SerializeField] private GameObject HitEffectPrefab;
    [SerializeField] private GameObject ParticeEffect;
    [SerializeField] private GameObject BulletPrefab;
    [SerializeField] private Text ammoText;
    private Player_Controller playerControllerScirpt;
    public int shootingStyle = 2;
    public int weaponDamage = 10;
    public int ammoUsage = 1;

    private void Start() 
    {
        playerControllerScirpt = GameObject.Find("Player").GetComponent<Player_Controller>();
    }

    private void Update()
    {
        if (!UI_PauseMenu.GameIsPaused)
        {
            if (Input.GetMouseButtonDown(0) && Player_Ammo >= 1)
            {
                if (shootingStyle == 1)
                {
                    ShootingByRaycast();
                }
                if (shootingStyle == 2)
                {
                    ShootingByAddforce();
                }
            }
            ammoText.text = "Ammo: " + Player_Ammo.ToString();
        }    
    }

    private void ShootingByRaycast()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(Fire_Point.position, Fire_Point.up);
        if (hitInfo)
        {
            Line_Renderer.SetPosition(0, Fire_Point.position);
            Line_Renderer.SetPosition(1, hitInfo.point);
            if (hitInfo.collider.tag == "Enemy")
            {
                hitInfo.collider.GetComponent<Enemy_Controller>().TakeDamage(weaponDamage, transform.up);
                GameObject EnemyHit = Instantiate(ParticeEffect, hitInfo.point, hitInfo.transform.rotation) as GameObject;
                Destroy(EnemyHit, 0.3f);

            }
            GameObject HitEffect = Instantiate(HitEffectPrefab, hitInfo.point, transform.rotation) as GameObject;
            Destroy(HitEffect, 0.1f);
        }
        else
        {
            GameObject HitEffect = Instantiate(HitEffectPrefab, hitInfo.point, transform.rotation) as GameObject;
            Destroy(HitEffect, 0.1f);
            Line_Renderer.SetPosition(0, Fire_Point.position);
            Line_Renderer.SetPosition(1, Fire_Point.position + Fire_Point.up * 100);
        }
        StartCoroutine(Delay());
        Player_Ammo -= ammoUsage;
    }

    private void ShootingByAddforce()
    {
        GameObject Bullet = Instantiate(BulletPrefab, Fire_Point.position, transform.rotation);
        Bullet.GetComponent<Rigidbody2D>().AddForce(Fire_Point.up * 20f, ForceMode2D.Impulse);
        Player_Ammo -= ammoUsage;
    }

    IEnumerator Delay()
    {
        Line_Renderer.enabled = true;
        yield return new WaitForSeconds(0.02f);
        Line_Renderer.enabled = false;
    }

    public void PlayerAmmo(int ammoAmount)
    {
        Player_Ammo += ammoAmount;
    }
}
