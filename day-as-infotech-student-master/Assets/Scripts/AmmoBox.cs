﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{

    [SerializeField] private int Ammo;
    [SerializeField] private GameObject ParticleSys;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameObject Sys = Instantiate(ParticleSys, transform.position, Quaternion.identity) as GameObject;
            Destroy(Sys, 2f);
            collision.GetComponent<Player_Shooting>().PlayerAmmo(Ammo);
            Destroy(this.gameObject);
        }
    }
}
