﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinuxMissleScript : MonoBehaviour
{
    public float speed;
    private Transform Player;
    private Vector2 target;


    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        target = new Vector2(Player.position.x, Player.position.y);

    }


    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (transform.position.x == target.x && transform.position.y == target.y)
        {
            DestroyProjectile();

        }
        Vector2 direction = new Vector2(transform.position.x - Player.transform.position.x, transform.position.y - Player.transform.position.y) * -1;
        transform.right = direction;

    }


    private void DestroyProjectile()
    {
        Destroy(gameObject, 0.1f);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            DestroyProjectile();


        }
    }
}
