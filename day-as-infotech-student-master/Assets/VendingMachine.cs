﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMachine : MonoBehaviour
{
	[SerializeField] private GameObject shop;
	[SerializeField] private GameObject popupInfo;
 	private bool playerIsNear = false;

	private void Update()
	{
		if (playerIsNear & Input.GetKeyDown(KeyCode.E))
		{
			if (shop.activeSelf == false)
			{
				shop.SetActive(true);
				popupInfo.SetActive(false);
			}
			else
			{
				shop.SetActive(false);
				popupInfo.SetActive(true);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			playerIsNear = true;
			popupInfo.SetActive(true);
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			shop.SetActive(false);
			playerIsNear = false;
			popupInfo.SetActive(false);
		}
	}
}
