﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_VendingMachine : MonoBehaviour
{
	private int currentPlayerHealth;
	private int currentPlayerMoney;
	private int maxPlayerHealth;
	[SerializeField] private GameObject playerObject;

	private void Start()
	{
		maxPlayerHealth = playerObject.GetComponent<Player_Controller>().maxHealth;
	}

	public void HealthBuy()
	{
		currentPlayerMoney = playerObject.GetComponent<Player_Controller>().playerMoney;
		currentPlayerHealth = playerObject.GetComponent<Player_Controller>().currentHealth;

		if (currentPlayerMoney >= 10)
		{
			if (currentPlayerHealth < maxPlayerHealth)
			{
				currentPlayerHealth += 20;
				currentPlayerMoney -= 10;

				playerObject.GetComponent<Player_Controller>().PlayerHealthUpdate(currentPlayerHealth);
				playerObject.GetComponent<Player_Controller>().PlayerMoneyDisplay(currentPlayerMoney);
			}
			else
			{
				Debug.Log("You have full HP.");
			}

		}
		else
		{
			Debug.Log("You don't have enough money.");
		}
	}
}
