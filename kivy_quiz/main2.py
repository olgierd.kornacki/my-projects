from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
import json


class Questions:
    questions = open("words.txt", "r")
    questions = json.loads(questions .read())

class MainWindow(Screen):
    ab1 = "Start"

class SecondWindow(Screen):
    def Add_Question(self):
        words = []
        try:
            f = open("words.txt", "r")
            words = json.loads(f.read())
            f.close()
        except:
            pass
        words1 = [self.ids.an1_input.text,self.ids.an2_input.text,self.ids.an3_input.text,self.ids.question_input.text,self.ids.CAn_input.text]
        for record in words1:
            if record == "":
                print("___Invalid Input___")
                return
        words.append(words1)

        try:
            f = open("words.txt", "w")
            f.write(json.dumps(words, indent=2))
            f.close()
        except:
            print("failed to write file")

class WindowManager(ScreenManager):
    points = 0
    n = 0
    Actual_Question = None
    CorrectAnswer = False
    words = []
    def Next_Question(self,instnace):
        if(WindowManager.Actual_Question == None):
            WindowManager.Actual_Question = Questions.questions[WindowManager.n]

        if(instnace.text == WindowManager.Actual_Question[4]):
            WindowManager.CorrectAnswer = True
            print("output:", WindowManager.Actual_Question[4] ,"is correct answer ")
        else:
            WindowManager.CorrectAnswer = False

        try:
            WindowManager.n += 1
            WindowManager.Actual_Question = Questions.questions[WindowManager.n]
        except:
            print("______________(Database empty)______________")

kv = Builder.load_file("my.kv")
class MyMainApp(App):


    def build(self):
        return kv

if __name__ == "__main__":
    MyMainApp().run()